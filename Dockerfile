FROM openjdk:17
VOLUME /tmp
COPY build/libs/mimic-*.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar", "--spring.profiles.active=prod"]
