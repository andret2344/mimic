[[_TOC_]]

# Intro

Mimic is a simple restful api that provides generic user data operations.

# How?

## Generating token

To use the service you first need to [generate JWT](#post-token). Once you obtained your token, you'll need to provide
it as Authentication Header for all future requests. See [Endpoints](#endpoints)
for a list of possible request.

## User entity

User entity consists of the following fields:

| Name         | Type          |
|--------------|---------------|
| id           | Long          |
| firstName    | String        |
| lastName     | String        |
| age          | Integer       |
| creationDate | LocalDateTime |
| subject      | String        |

All fields are nullable, `id` and `subject` fields should be omitted since service takes care of assigning their values.

# Endpoints

## `POST` Token

Sends an email containing the JWT to the given recipient <br>
*/token/generate/{email}*

| Parameter | Type   | Description                                    |
|-----------|--------|------------------------------------------------|
| {email}   | string | An email address to be used for authentication |

| Success status code |
|:-------------------:|
|     201 CREATED     |

## `GET` User list

Returns list of user entities <br>
*/users*

| Parameter      | Type   | Description                     |
|----------------|--------|---------------------------------|
| Authentication | Header | JWT required for authentication |

| Success status code |
|:-------------------:|
|       200 OK        |

## `GET` Specific user

Returns user entity with given ID <br>
*/users/{id}*

| Parameter      | Type   | Description                     |
|----------------|--------|---------------------------------|
| Authentication | Header | JWT required for authentication |
| {id}           | long   | The ID of user entity           |

| Success status code | Error status code |
|:-------------------:|:-----------------:|
|       200 OK        |   404 NOT FOUND   |

## `POST` Random user

Creates randomized user entity, saves and returns it <br>
*/users/random*

| Parameter      | Type   | Description                     |
|----------------|--------|---------------------------------|
| Authentication | Header | JWT required for authentication |

| Success status code |
|:-------------------:|
|     201 Created     |

## `POST` Random users

Creates *n* randomized user entities, saves them and returns a list <br>
*/users/random/{n}*

| Parameter      | Type    | Description                         |
|----------------|---------|-------------------------------------|
| Authentication | Header  | JWT required for authentication     |
| {n}            | integer | Number of user entities to generate |

| Success status code | Error status code |
|:-------------------:|:-----------------:|
|     201 CREATED     |  400 BAD REQUEST  |

## `POST` Specific user

Saves given user entity <br>
*/users*

| Parameter      | Type   | Description                     |
|----------------|--------|---------------------------------|
| Authentication | Header | JWT required for authentication |
| User entity    | Body   | User entity to be saved         |

| Success status code | Error status code |
|:-------------------:|:-----------------:|
|     201 CREATED     |  400 BAD REQUEST  |

## `PUT` Specific user

Fully updates user entity with given *id* with values passed in *user body*. Empty values are set as null. <br>
*/users/{id}*

| Parameter      | Type    | Description                         |
|----------------|---------|-------------------------------------|
| Authentication | Header  | JWT required for authentication     |
| User entity    | Body    | User entity to be used for update   |
| {id}           | integer | The ID of user entity to be updated |

| Success status code | Error status code |
|:-------------------:|:-----------------:|
|       200 OK        |   404 NOT FOUND   |

## `PATCH` Specific user

Partially updates user entity with given *id* with values passed in *user body*. Empty values are omitted. <br>
*/users/{id}*

| Parameter      | Type    | Description                         |
|----------------|---------|-------------------------------------|
| Authentication | Header  | JWT required for authentication     |
| User entity    | Body    | User entity to be used for update   |
| {id}           | integer | The ID of user entity to be updated |

| Success status code | Error status code |
|:-------------------:|:-----------------:|
|       200 OK        |   404 NOT FOUND   |

## `DELETE` User

Deletes user entity with given *id* <br>
*/users/{id}*

| Parameter      | Type    | Description                         |
|----------------|---------|-------------------------------------|
| Authentication | Header  | JWT required for authentication     |
| {id}           | integer | The ID of user entity to be deleted |

| Success status code | Error status code |
|:-------------------:|:-----------------:|
|       200 OK        |   404 NOT FOUND   |

# Author

Mimic is an open source service created by [Andrzej Chmiel](https://andret.eu/) (forked from abandoned
by [Krzysztof Fryta](http://kr1stoffer.net)).
