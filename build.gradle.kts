plugins {
	idea
	java
	jacoco
	id("org.springframework.boot") version "3.3.1"
	id("io.spring.dependency-management") version "1.1.5"
	id("org.barfuin.gradle.jacocolog") version "3.1.0"
}

repositories {
	mavenCentral()
}

dependencies {
	// spring boot
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-mail")
	implementation("org.springframework.boot:spring-boot-starter-security")

	// spring
	implementation("org.springframework.security:spring-security-core")

	// swagger
	implementation("org.springdoc:springdoc-openapi-starter-webmvc-ui:2.5.0")

	// database
	implementation("org.hibernate.validator:hibernate-validator:8.0.1.Final")
	implementation("com.mysql:mysql-connector-j:8.4.0")

	// jwt
	implementation("com.auth0:java-jwt:4.4.0")

	// tests
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.testng:testng:7.10.2")
	testImplementation("org.assertj:assertj-core:3.26.0")
	testImplementation("org.mockito:mockito-core:5.12.0")
	testImplementation("org.mockito:mockito-testng:0.5.2")
}

tasks {
	compileJava {
		sourceCompatibility = "17"
		targetCompatibility = "17"
	}

	jar {
		enabled = false
	}

	test {
		useTestNG()
		finalizedBy(jacocoTestCoverageVerification)
	}

	jacocoTestReport {
		classDirectories.setFrom(
			files(classDirectories.files.map {
				fileTree(it).matching {
					exclude(
						"**/MimicApplication.*",
						"**/controller/**"
					)
				}
			})
		)
	}

	jacocoTestCoverageVerification {
		violationRules {
			rule {
				classDirectories.setFrom(jacocoTestReport.get().classDirectories)
				limit {
					minimum = "0.7".toBigDecimal()
				}
			}
		}
	}
}
