package eu.andret.mimic.service;

import com.auth0.jwt.interfaces.DecodedJWT;
import eu.andret.mimic.entity.User;
import eu.andret.mimic.exception.UserCapReachedException;
import eu.andret.mimic.repository.UserRepository;
import eu.andret.mimic.utils.AutoAuth;
import eu.andret.mimic.utils.UserCap;
import org.assertj.core.api.ThrowableAssert;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.testng.MockitoTestNGListener;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@Listeners(MockitoTestNGListener.class)
public class UserServiceTest {
	private static final String SUBJECT = "testSubject";
	private final DecodedJWT decodedJWT = mock(DecodedJWT.class);
	@Mock
	private UserRepository userRepository;
	@Mock
	private TokenService tokenService;
	@InjectMocks
	private UserService userService;

	@BeforeMethod
	void setupTest(@NonNull final Method method) {
		SecurityContextHolder.clearContext();
		if (method.isAnnotationPresent(AutoAuth.class)) {
			when(decodedJWT.getSubject()).thenReturn(method.getAnnotation(AutoAuth.class).subject());

			final UsernamePasswordAuthenticationToken authenticationToken =
					new UsernamePasswordAuthenticationToken(decodedJWT, null, null);

			final SecurityContext securityContext = new SecurityContextImpl();
			securityContext.setAuthentication(authenticationToken);
			SecurityContextHolder.setContext(securityContext);
		}

		if (method.isAnnotationPresent(UserCap.class)) {
			when(tokenService.getUserCap(decodedJWT)).thenReturn(method.getAnnotation(UserCap.class).value());
		}
	}

	@Test
	@AutoAuth
	@UserCap(0)
	void addRandomUserAboveUserCap() {
		// when
		final ThrowableAssert.ThrowingCallable callable = () -> userService.getRandomizedUser();

		// then
		assertThatThrownBy(callable).isInstanceOf(UserCapReachedException.class);
	}

	@Test
	@AutoAuth
	@UserCap(10)
	void addMultipleUsersAboveUserCap() {
		// when
		final ThrowableAssert.ThrowingCallable callable = () -> userService.getRandomizedUsers(20);

		// then
		assertThatThrownBy(callable).isInstanceOf(UserCapReachedException.class);
	}

	@Test
	@AutoAuth
	@UserCap(0)
	void postUserAboveUserCap() {
		// when
		final ThrowableAssert.ThrowingCallable callable = () -> userService.post(new User());

		// then
		assertThatThrownBy(callable).isInstanceOf(UserCapReachedException.class);
	}

	@Test
	@AutoAuth
	@UserCap
	void testIfRandomUserIsIndeedRandom() {
		// given
		final int size = 100;
		when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

		// when
		final List<User> users = userService.getRandomizedUsers(size);

		// then
		assertThat(users)
				.hasSize(size)
				.doesNotHaveDuplicates();
	}

	@Test
	@AutoAuth
	@UserCap
	void testIfUserIsNotNull() {
		// given
		when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

		// when
		final User user = userService.getRandomizedUser();

		// then
		assertThat(user).isNotNull();
	}

	@Test
	@AutoAuth
	@UserCap
	void testIfRandomNameIsNotEmptyOrNull() {
		//given
		when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

		// when
		final User user = userService.getRandomizedUser();

		// then
		assertThat(user.getFirstName())
				.isNotNull()
				.isNotEmpty();
		assertThat(user.getLastName())
				.isNotNull()
				.isNotEmpty();
	}

	@Test
	@AutoAuth
	void generateNegativeNumberOfUsers() {
		// when
		final ThrowableAssert.ThrowingCallable callable = () -> userService.getRandomizedUsers(-1);

		// then
		assertThatThrownBy(callable).isInstanceOf(IllegalArgumentException.class);
	}

	@Test
	@AutoAuth
	@UserCap
	void testIfUserIsSaved() {
		// given
		when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

		// when
		final User user = userService.getRandomizedUser();

		// then
		verify(userRepository).save(user);
	}

	@Test
	@AutoAuth
	@UserCap
	void testIfUsersAreSaved() {
		// given
		final int invocations = 10;
		when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

		// when
		userService.getRandomizedUsers(invocations);

		// then
		verify(userRepository, times(invocations)).save(any(User.class));
	}

	@Test
	@AutoAuth
	void testDatabaseForUser() {
		//given
		final long id = 0;

		// when
		userService.get(id);

		// then
		verify(userRepository).findByIdAndSubject(id, SUBJECT);
	}

	@Test
	@AutoAuth
	void testDatabaseForAllUsers() {
		// when
		userService.getAll();

		// then
		verify(userRepository).findBySubject(SUBJECT);
	}

	@Test
	@AutoAuth
	void testDatabaseForDeleteQuery() {
		// given
		final long id = 0;

		// when
		userService.delete(id);

		// then
		verify(userRepository).deleteByIdAndSubject(id, SUBJECT);
	}

	@Test
	@AutoAuth
	@UserCap
	void postNewUser() {
		// given
		when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));
		final User user = userService.getRandomizedUser();

		// when
		final boolean hasBeenPosted = userService.post(user);

		// then
		assertThat(hasBeenPosted).isTrue();
	}

	@Test
	@AutoAuth
	@UserCap
	void postNullUser() {
		// when
		final boolean hasBeenPost = userService.post(null);

		// then
		assertThat(hasBeenPost).isFalse();
	}

	@Test
	@AutoAuth
	void putNonExistingUser() {
		// given
		final User user = mock(User.class);

		// when
		final boolean hasBeenPut = userService.put(1L, user);

		// then
		assertThat(hasBeenPut).isFalse();
	}

	@Test
	@AutoAuth(subject = "nerzhul@gmail.com")
	void putNewUser() {
		// given
		final ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
		final long id = 1L;
		final String subject = "nerzhul@gmail.com";
		final User oldUser = new User()
				.withId(id)
				.withFirstName("Arthas")
				.withLastName("Menethil")
				.withAge(24)
				.withCreationDate(LocalDateTime.now())
				.withSubject(subject);
		final User newUser = new User()
				.withId(2L)
				.withFirstName("Bolvar")
				.withLastName("Fordragon")
				.withAge(30)
				.withCreationDate(LocalDateTime.now())
				.withSubject("doNotUpdateThisField@10minutemail.com");

		when(userRepository.findByIdAndSubject(id, subject)).thenReturn(oldUser);
		when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

		// when
		final boolean hasBeenPut = userService.put(id, newUser);
		verify(userRepository).save(captor.capture());


		// then
		assertThat(hasBeenPut).isTrue();
		assertThat(captor.getValue())
				.isEqualTo(newUser
						.withId(id)
						.withSubject(subject));
	}

	@Test
	@AutoAuth
	void patchNullUser() {
		// when
		final boolean hasBeenPatched = userService.patch(0, null);

		// then
		assertThat(hasBeenPatched).isFalse();
	}

	@Test
	@AutoAuth
	void patchUserWithAllNulls() {
		// given
		final ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
		final long id = 1;
		final String subject = "testSubject";
		final User oldUser = new User()
				.withId(id)
				.withFirstName("a")
				.withLastName("b")
				.withAge(100)
				.withCreationDate(LocalDateTime.now())
				.withSubject(subject);
		final User newUser = new User()
				.withId(2L)
				.withFirstName(null)
				.withLastName(null)
				.withAge(null)
				.withCreationDate(null)
				.withSubject(null);

		when(userRepository.findByIdAndSubject(id, subject)).thenReturn(oldUser);
		when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

		// when
		final boolean hasBeenPatched = userService.patch(id, newUser);
		verify(userRepository).save(captor.capture());

		// then
		assertThat(hasBeenPatched).isTrue();
		assertThat(captor.getValue()).isEqualTo(oldUser);
	}

	@Test
	@AutoAuth(subject = "Minecraft")
	void patchAllFieldsOfUser() {
		// given
		final ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
		final long id = 1;
		final String subject = "Minecraft";
		final User oldUser = new User()
				.withId(id)
				.withFirstName(subject)
				.withLastName("Persson")
				.withAge(724)
				.withCreationDate(LocalDateTime.now())
				.withSubject("Minecraft");
		final User newUser = new User()
				.withId(2L)
				.withFirstName("Some")
				.withLastName("Person")
				.withAge(24)
				.withCreationDate(LocalDateTime.now())
				.withSubject("marek.cukiergora@naszaklasa.pl");

		when(userRepository.findByIdAndSubject(id, subject)).thenReturn(oldUser);
		when(userRepository.save(any(User.class))).thenAnswer(invocation -> invocation.getArgument(0));

		// when
		final boolean hasBeenPatched = userService.patch(id, newUser);
		verify(userRepository).save(captor.capture());

		// then
		assertThat(hasBeenPatched).isTrue();
		assertThat(captor.getValue())
				.isEqualTo(newUser
						.withId(id)
						.withSubject(subject));
	}
}
