package eu.andret.mimic.service;

import com.auth0.jwt.exceptions.JWTVerificationException;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.ThrowableAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
public class TokenServiceTest {
	private TokenService tokenService;

	@BeforeTest
	void initialize() {
		tokenService = new TokenService("test-secret");
	}

	@Test
	void tokenGenerated() {
		// given
		final String subject = "testSubject";

		// when
		final String token = tokenService.generate(subject);

		// then
		Assertions.assertThat(token)
				.isNotNull()
				.isNotEmpty();
	}

	@Test
	void authNullToken() {
		// when
		final ThrowableAssert.ThrowingCallable callable = () -> tokenService.auth(null);

		// then
		assertThatThrownBy(callable).isInstanceOf(JWTVerificationException.class);
	}

	@Test
	void authEmptyToken() {
		// when
		final ThrowableAssert.ThrowingCallable callable = () -> tokenService.auth("");

		// then
		assertThatThrownBy(callable).isInstanceOf(JWTVerificationException.class);
	}

	@Test
	void authValidToken() {
		// given
		final String token = tokenService.generate("testSubject");

		// when
		final ThrowableAssert.ThrowingCallable callable = () -> tokenService.auth(token);

		// then
		assertThatNoException().isThrownBy(callable);
	}
}
