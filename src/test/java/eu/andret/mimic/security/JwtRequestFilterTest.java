package eu.andret.mimic.security;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import eu.andret.mimic.service.TokenService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletResponse;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.testng.MockitoTestNGListener;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@Listeners(MockitoTestNGListener.class)
public class JwtRequestFilterTest {
	@Mock
	private TokenService tokenService;
	@InjectMocks
	private JwtRequestFilter jwtRequestFilter;

	@Test
	void validTokenAuthorized() throws ServletException, IOException {
		// Given
		final MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader("Authorization", "Bearer validToken");
		final MockHttpServletResponse response = new MockHttpServletResponse();
		final FilterChain filterChain = new MockFilterChain();

		final DecodedJWT decodedJWT = mock(DecodedJWT.class);
		when(tokenService.auth(anyString())).thenReturn(decodedJWT);

		// When
		jwtRequestFilter.doFilterInternal(request, response, filterChain);

		// Then
		assertThat(SecurityContextHolder.getContext().getAuthentication()).isNotNull();
		assertThat(SecurityContextHolder.getContext().getAuthentication())
				.isInstanceOf(UsernamePasswordAuthenticationToken.class);
		assertThat(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).isEqualTo(decodedJWT);
	}

	@Test
	void invalidTokenUnauthorized() throws ServletException, IOException {
		// Given
		final MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader("Authorization", "Bearer invalidToken");
		final MockHttpServletResponse response = new MockHttpServletResponse();
		final FilterChain filterChain = new MockFilterChain();

		when(tokenService.auth(anyString())).thenThrow(new JWTVerificationException("Invalid token"));

		// When
		jwtRequestFilter.doFilterInternal(request, response, filterChain);

		// Then
		assertThat(response.getStatus()).isEqualTo(HttpServletResponse.SC_UNAUTHORIZED);
		assertThat(response.getContentAsString()).isEqualTo("Invalid token");
	}

	@Test
	void missingToken() throws ServletException, IOException {
		// Given
		final MockHttpServletRequest request = new MockHttpServletRequest();
		final MockHttpServletResponse response = new MockHttpServletResponse();
		final FilterChain filterChain = mock(FilterChain.class);

		// When
		jwtRequestFilter.doFilterInternal(request, response, filterChain);

		// Then
		verify(filterChain, times(1)).doFilter(request, response);
		assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
	}
}
