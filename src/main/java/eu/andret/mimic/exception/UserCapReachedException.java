package eu.andret.mimic.exception;

import org.springframework.lang.Nullable;

public class UserCapReachedException extends RuntimeException {
	public UserCapReachedException() {
		this("Cannot add more users, because the user cap has been reached.");
	}

	public UserCapReachedException(@Nullable final String message) {
		super(message);
	}
}
