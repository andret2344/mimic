package eu.andret.mimic.exception;

import jakarta.mail.MessagingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.mail.MailSendException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class GlobalExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<Map<String, Object>> handleAllUncaughtExceptions(@NonNull final RuntimeException exception) {
		logger.error("Unhandled exception occurred", exception);
		final Map<String, Object> response = getExceptionBody(HttpStatus.INTERNAL_SERVER_ERROR, "Something went wrong while processing your request. Our engineering team is already working on resolving this issue.");
		return ResponseEntity
				.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.body(response);
	}

	@NonNull
	@ExceptionHandler({MailSendException.class, MessagingException.class})
	public ResponseEntity<Map<String, Object>> handleInvalidEmail(@NonNull final RuntimeException exception) {
		logger.error("Mailer exception occurred", exception);
		return ResponseEntity
				.status(HttpStatus.BAD_REQUEST)
				.body(getExceptionBody(HttpStatus.BAD_REQUEST, "Something wen wrong while trying to send email. Please check if provided email address is correct"));
	}

	@NonNull
	private Map<String, Object> getExceptionBody(@NonNull final HttpStatus httpStatus, @NonNull final String message) {
		final Map<String, Object> response = new HashMap<>();
		response.put("status", httpStatus.value());
		response.put("message", message);
		return response;
	}
}
