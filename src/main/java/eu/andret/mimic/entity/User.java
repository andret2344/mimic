package eu.andret.mimic.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public final class User {
	@Id
	@GeneratedValue
	@Schema(description = "Auto-generated value", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
	private final Long id;
	@NotBlank
	private final String firstName;
	@NotBlank
	private final String lastName;
	@NotNull
	@Min(value = 18, message = "Age must be above or equal 18.")
	@Max(value = 101, message = "Age must be below or equal 101.")
	private final Integer age;
	@NotNull
	private final LocalDateTime creationDate;
	@Schema(description = "Auto-assigned value", requiredMode = Schema.RequiredMode.NOT_REQUIRED)
	private final String subject;

	public User() {
		this(null, null, null, null, null, null);
	}

	private User(final Long id, final String firstName, final String lastName, final Integer age, final LocalDateTime creationDate, final String subject) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.creationDate = creationDate;
		this.subject = subject;
	}

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Integer getAge() {
		return age;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public String getSubject() {
		return subject;
	}

	@NonNull
	public User withId(final Long id) {
		return new User(id, firstName, lastName, age, creationDate, subject);
	}

	@NonNull
	public User withFirstName(final String firstName) {
		return new User(id, firstName, lastName, age, creationDate, subject);
	}

	@NonNull
	public User withLastName(final String lastName) {
		return new User(id, firstName, lastName, age, creationDate, subject);
	}

	@NonNull
	public User withAge(final Integer age) {
		return new User(id, firstName, lastName, age, creationDate, subject);
	}

	@NonNull
	public User withCreationDate(final LocalDateTime creationDate) {
		return new User(id, firstName, lastName, age, creationDate, subject);
	}

	@NonNull
	public User withSubject(final String subject) {
		return new User(id, firstName, lastName, age, creationDate, subject);
	}

	@Override
	@Generated("IntelliJ")
	public boolean equals(@Nullable final Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null || obj.getClass() != getClass()) {
			return false;
		}
		final User that = (User) obj;
		return Objects.equals(id, that.id) &&
				Objects.equals(firstName, that.firstName) &&
				Objects.equals(lastName, that.lastName) &&
				Objects.equals(age, that.age) &&
				Objects.equals(creationDate, that.creationDate) &&
				Objects.equals(subject, that.subject);
	}

	@Override
	@Generated("Intellij")
	public int hashCode() {
		return Objects.hash(id, firstName, lastName, age, creationDate, subject);
	}

	@NonNull
	@Override
	@Generated("IntelliJ")
	public String toString() {
		return "UserEntity[" +
				"id=" + id + ", " +
				"firstName=" + firstName + ", " +
				"lastName=" + lastName + ", " +
				"age=" + age + ", " +
				"creationDate=" + creationDate + ", " +
				"subject=" + subject + ']';
	}
}
