package eu.andret.mimic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.lang.NonNull;

@SpringBootApplication
public class MimicApplication {
	public static void main(@NonNull final String[] args) {
		SpringApplication.run(MimicApplication.class, args);
	}
}
