package eu.andret.mimic.service;

import eu.andret.mimic.config.ThymeleafConfiguration;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import java.nio.charset.StandardCharsets;

@Service
public class EmailService {
	private static final String TOKEN_EMAIL_SUBJECT = "Your Mimic authentication token";
	private static final String NOREPLY_ADDRESS = "noreply@andret.eu";
	private final JavaMailSender mailSender;
	private final ThymeleafConfiguration emailConfiguration;

	public EmailService(final JavaMailSender mailSender, final ThymeleafConfiguration emailConfiguration) {
		this.mailSender = mailSender;
		this.emailConfiguration = emailConfiguration;
	}

	/**
	 * Sends an email containing JWT, using parsed HTML template.
	 *
	 * @param to the email recipient
	 * @param token the JWT.
	 * @throws MessagingException in case of failure when sending the message.
	 */
	public void sendTokenEmail(final String to, final String token) throws MessagingException {
		final Context context = new Context();
		context.setVariable("jwt", token);

		final MimeMessage message = mailSender.createMimeMessage();
		final MimeMessageHelper messageHelper = new MimeMessageHelper(message, true, StandardCharsets.UTF_8.name());
		messageHelper.setFrom(NOREPLY_ADDRESS);
		messageHelper.setTo(to);
		messageHelper.setSubject(TOKEN_EMAIL_SUBJECT);

		final String htmlContent = emailConfiguration.thymeleafTemplateEngine().process("token-email-template", context);
		messageHelper.setText(htmlContent, true);
		mailSender.send(message);
	}
}
