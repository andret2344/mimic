package eu.andret.mimic.service;

import com.auth0.jwt.interfaces.DecodedJWT;
import eu.andret.mimic.entity.User;
import eu.andret.mimic.exception.UserCapReachedException;
import eu.andret.mimic.repository.UserRepository;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserService {
	private static final Random random = new Random();
	private static final int RANDOM_NAME_MIN_LENGTH = 2;
	private static final int RANDOM_NAME_MAX_LENGTH = 33;
	private final UserRepository userRepository;
	private final TokenService tokenService;

	public UserService(@NonNull final UserRepository userRepository, @NonNull final TokenService tokenService) {
		this.userRepository = userRepository;
		this.tokenService = tokenService;
	}

	/**
	 * Creates a user entity with a random name, surname and age, and creation date set to the moment of generation.
	 *
	 * @return the saved instance of the created user entity with an auto-generated id.
	 * @throws UserCapReachedException if the user cap has been reached for this token.
	 */
	@NonNull
	public User getRandomizedUser() {
		final DecodedJWT decodedJWT = getDecodedJwt();
		final int userCap = tokenService.getUserCap(decodedJWT);

		if (getUserAmount(decodedJWT.getSubject()) >= userCap) {
			throw new UserCapReachedException();
		}

		final User user = getUser().withSubject(decodedJWT.getSubject());
		return userRepository.save(user);
	}

	/**
	 * Creates a list containing a given amount of randomly generated user entities.
	 *
	 * @param amount the number of users to be generated.
	 * @return a list of randomly generated user entities.
	 * @throws UserCapReachedException if there are no
	 * more slots to generate a given amount of users.
	 * @see #getRandomizedUser()
	 */
	public List<User> getRandomizedUsers(final int amount) {
		final DecodedJWT decodedJWT = getDecodedJwt();
		final String subject = decodedJWT.getSubject();
		final int userCap = tokenService.getUserCap(decodedJWT);
		final int userSlots = userCap - getUserAmount(subject);

		if (amount > userSlots) {
			throw new UserCapReachedException(
					String.format("Cannot generate %d users, only %d user slots remain.", amount, userSlots));
		}

		return Stream.generate(this::getRandomizedUser)
				.limit(amount)
				.toList();
	}

	/**
	 * Returns a user entity with the given id from the database.
	 *
	 * @param id the id of the user entity to be returned.
	 * @return the user entity with the given id or null if no such entity exists.
	 */
	public User get(final long id) {
		return userRepository.findByIdAndSubject(id, getJwtSubject());
	}

	/**
	 * Returns a list containing all user entities of the given token bearer.
	 *
	 * @return a list containing all user entities of the given token bearer
	 * or an empty list if no entities could be found.
	 * @see #get(long)
	 */
	public List<User> getAll() {
		return userRepository.findBySubject(getJwtSubject());
	}

	/**
	 * Adds new user entity to the database. Custom id and subject fields are omitted.
	 *
	 * @param user the user entity passed in request body.
	 * @return {@code true} if given non-null user was added.
	 * @throws UserCapReachedException if the user cap has been reached for this token.
	 * @see #getRandomizedUser()
	 */
	public boolean post(@Nullable final User user) {
		final DecodedJWT decodedJWT = getDecodedJwt();
		final String subject = decodedJWT.getSubject();
		final int userCap = tokenService.getUserCap(decodedJWT);

		if (getUserAmount(subject) >= userCap) {
			throw new UserCapReachedException();
		}

		return Optional.ofNullable(user)
				.map(u -> userRepository.save(u
						.withId(null)
						.withSubject(subject)))
				.isPresent();
	}

	/**
	 * Fully updates user entity with the given id.
	 *
	 * @param id the id of the user entity to be updated.
	 * @param user the user entity passed to the request body.
	 * @return {@code true} if an entity with the given id exists and could be updated, {@code false} if no such entity exists.
	 * @see #patch(long, User) to partially update user entity
	 */
	public boolean put(final long id, @NonNull final User user) {
		return Optional.ofNullable(get(id))
				.map(u -> userRepository.save(new User()
						.withId(id)
						.withFirstName(user.getFirstName())
						.withLastName(user.getLastName())
						.withAge(user.getAge())
						.withCreationDate(user.getCreationDate())
						.withSubject(u.getSubject())))
				.isPresent();
	}

	/**
	 * Partially updates user entity with the given id. Fields with a null value will be omitted.
	 *
	 * @param id the id of the user entity to be updated.
	 * @param user the user entity passed to the request body
	 * @return {@code true} if an entity with the given id exists and was updated, {@code false} otherwise.
	 * @see #put(long, User) to fully update user entity
	 */
	public boolean patch(final long id, @Nullable final User user) {
		if (user == null) {
			return false;
		}
		return Optional.ofNullable(get(id))
				.map(u -> userRepository.save(new User()
						.withId(id)
						.withFirstName(Optional.ofNullable(user.getFirstName()).orElse(u.getFirstName()))
						.withLastName(Optional.ofNullable(user.getLastName()).orElse(u.getLastName()))
						.withAge(Optional.ofNullable(user.getAge()).orElse(u.getAge()))
						.withCreationDate(Optional.ofNullable(user.getCreationDate()).orElse(u.getCreationDate()))
						.withSubject(u.getSubject())))
				.isPresent();
	}

	/**
	 * Removes a user entity with the given id from the database.
	 *
	 * @param id the id of the user entity to be removed.
	 * @return {@code true} if an entity with the given id was removed, {@code false} otherwise.
	 */
	public boolean delete(final long id) {
		return userRepository.deleteByIdAndSubject(id, getJwtSubject()) > 0;
	}

	@NonNull
	private String getRandomName(final int length) {
		final String name = Stream.generate(() -> (char) random.nextInt('a', 'z'))
				.limit(length)
				.map(String::valueOf)
				.collect(Collectors.joining());
		return name.substring(0, 1).toUpperCase() + name.substring(1);
	}

	@NonNull
	private String getRandomName() {
		final int length = random.nextInt(RANDOM_NAME_MAX_LENGTH - RANDOM_NAME_MIN_LENGTH) + RANDOM_NAME_MIN_LENGTH;
		return getRandomName(length);
	}

	private int getRandomAge() {
		return random.nextInt(18, 101);
	}

	@NonNull
	private User getUser() {
		return new User()
				.withFirstName(getRandomName())
				.withLastName(getRandomName())
				.withAge(getRandomAge())
				.withCreationDate(LocalDateTime.now());
	}

	private int getUserAmount(final String subject) {
		return userRepository.findBySubject(subject).size();
	}

	@NonNull
	private DecodedJWT getDecodedJwt() {
		return Optional.of(SecurityContextHolder.getContext())
				.map(SecurityContext::getAuthentication)
				.map(Authentication::getPrincipal)
				.map(DecodedJWT.class::cast)
				.orElseThrow();
	}

	@NonNull
	private String getJwtSubject() {
		return getDecodedJwt().getSubject();
	}
}
