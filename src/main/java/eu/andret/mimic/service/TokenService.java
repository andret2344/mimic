package eu.andret.mimic.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TokenService {
	private static final String USER_CAP = "userCap";
	private final Algorithm algorithm;
	private final JWTVerifier jwtVerifier;

	public TokenService(@Value("${jwt.secret}") final String jwtSecret) {
		algorithm = Algorithm.HMAC512(jwtSecret);
		jwtVerifier = JWT.require(algorithm).build();
	}

	/**
	 * Generates the JWT based on the given the subject.
	 *
	 * @param subject the bearer of the JWT.
	 * @return the JWT used for authentication.
	 */
	@NonNull
	public String generate(@Nullable final String subject) {
		return JWT.create()
				.withSubject(subject)
				.withClaim(USER_CAP, 100)
				.sign(algorithm);
	}

	/**
	 * Authenticates the given token.
	 *
	 * @param token the JWT to be authenticated.
	 * @return the {@link DecodedJWT} containing information about the bearer.
	 */
	@NonNull
	public DecodedJWT auth(@Nullable final String token) {
		return Optional.ofNullable(token)
				.map(jwtVerifier::verify)
				.orElseThrow(() -> new JWTVerificationException("Could not verify provided token"));
	}

	/**
	 * Returns the maximum amount of users per token.
	 *
	 * @param decodedJWT the decoded token.
	 * @return the user cap of this token.
	 */
	public int getUserCap(@NonNull final DecodedJWT decodedJWT) {
		return decodedJWT.getClaim(USER_CAP).asInt();
	}
}
