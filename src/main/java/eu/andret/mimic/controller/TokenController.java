package eu.andret.mimic.controller;

import eu.andret.mimic.service.EmailService;
import eu.andret.mimic.service.TokenService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.mail.MessagingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/token")
@Tag(name = "Token controller", description = "Endpoints to manage tokens")
public class TokenController {
	private final TokenService tokenService;
	private final EmailService emailService;

	public TokenController(@NonNull final TokenService tokenService, @NonNull final EmailService emailService) {
		this.tokenService = tokenService;
		this.emailService = emailService;
	}

	@NonNull
	@PostMapping("/generate/{email}")
	public ResponseEntity<String> generateToken(@PathVariable @NonNull final String email) throws MessagingException {
		final String token = tokenService.generate(email);
		emailService.sendTokenEmail(email, token);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
}
