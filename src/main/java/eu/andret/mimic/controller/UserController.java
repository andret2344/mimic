package eu.andret.mimic.controller;

import eu.andret.mimic.entity.User;
import eu.andret.mimic.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/users")
@SecurityRequirement(name = "bearer-jwt")
@Tag(name = "Users controller", description = "Endpoints to manage users per token")
public class UserController {
	private final UserService userService;

	public UserController(final UserService userService) {
		this.userService = userService;
	}

	@GetMapping
	public ResponseEntity<List<User>> getAllUsers() {
		return ResponseEntity.ok(userService.getAll());
	}

	@GetMapping("/{id}")
	public ResponseEntity<User> getUser(@PathVariable final long id) {
		return Optional.ofNullable(userService.get(id))
				.map(ResponseEntity::ok)
				.orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	@PostMapping("/random")
	public ResponseEntity<User> addRandomUser() {
		return new ResponseEntity<>(userService.getRandomizedUser(), HttpStatus.CREATED);
	}

	@PostMapping("/random/{amount}")
	public ResponseEntity<List<User>> addRandomUsers(@PathVariable final int amount) {
		return new ResponseEntity<>(userService.getRandomizedUsers(amount), HttpStatus.CREATED);
	}

	@PostMapping
	public ResponseEntity<User> addUser(@RequestBody final User user) {
		userService.post(user);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	public ResponseEntity<User> putUser(@RequestBody final User user, @PathVariable final long id) {
		if (userService.put(id, user)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PatchMapping("/{id}")
	public ResponseEntity<User> patchUser(@RequestBody final User user, @PathVariable final long id) {
		if (userService.patch(id, user)) {
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<User> deleteUser(@PathVariable final long id) {
		if (userService.delete(id)) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
