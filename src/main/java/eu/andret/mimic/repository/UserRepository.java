package eu.andret.mimic.repository;

import eu.andret.mimic.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	@NonNull
	List<User> findBySubject(@NonNull String subject);

	@Nullable
	User findByIdAndSubject(long id, @NonNull String subject);

	@Modifying
	@Transactional
	int deleteByIdAndSubject(long id, @NonNull String subject);
}
