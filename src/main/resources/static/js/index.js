document.addEventListener('DOMContentLoaded', () => {
	const generate = document.querySelector('.submit-generate');
	const spinner = document.querySelector('.submit-spinner');
	const button = document.querySelector('.submit-button');
	const email = document.querySelector('.input-email');

	document.querySelector('form').addEventListener('submit', event => {
		event.preventDefault();
		const ok = document.querySelector('.submit-ok');

		generate.classList.add('d-none');
		spinner.classList.remove('d-none');
		email.setAttribute('disabled', '');
		button.setAttribute('disabled', '');

		const url = `/token/generate/${email.value}`;

		axios.post(url)
			.then(() => {
				spinner.classList.add('d-none');
				ok.classList.remove('d-none');
				button.classList.remove('btn-primary');
				button.classList.add('btn-success');
			})
			.catch(() => {
				generate.classList.remove('d-none');
				email.classList.add('is-invalid');
				spinner.classList.add('d-none');
				email.removeAttribute('disabled');
				button.removeAttribute('disabled');
			});
	});
});
